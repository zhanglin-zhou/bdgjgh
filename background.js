// When the extension is installed or upgraded ...
/*
chrome.runtime.onInstalled.addListener(function() {
  // Replace all rules ...
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    // With a new rule ...
    chrome.declarativeContent.onPageChanged.addRules([
      {
        // That ires when a page's URL contains a 'g' ...
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { urlContains: 'g' },
          })
        ],
        // And shows the extension's page action.
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });
});
*/

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
   if (request.method == "getFavorite") {
      var favorite = {};
      favorite.username = localStorage["username"];
      favorite.password = localStorage["password"];
      favorite.department = localStorage["department"];
      favorite.doctor = localStorage["doctor"];
      favorite.date = localStorage["date"];
      sendResponse(favorite);
   } else if (request.method == "saveLists" ) {
      var lists = request.value;
      localStorage["lists"] = JSON.stringify(lists);
      sendResponse(localStorage["lists"]);
   } else {
      sendResponse({}); // snub them.
   }
});