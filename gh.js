var fav_department = localStorage["department"];
var fav_doctor = localStorage["doctor"];
var fav_date = localStorage["date"];
var username = localStorage["username"];
var password = localStorage["password"];

var favorite = {};

function retrieve_tickets() {
   var found = 0;
   console.log("retrieve_tickets");
   $("a[onclick]").each(function() {
      var doctor = $(this).parent("td").prev("td").text().trim();
      var date = $(this).parent("td").prev("td").prev("td").text().trim();
      if ( (favorite.doctor == undefined || favorite.doctor == doctor) &&
           (favorite.date == undefined || favorite.date == date)) {
            found = 1;
            console.log("found ticket for doctor: " + doctor + " date: " + date);
            $(this)[0].click();
            return false;
           }
   });
   if (!found) {
      location.reload();
   }
}

function login() {
   if (favorite.username && favorite.password) {
      $("#mobile").val(favorite.username);
      $("#password").val(favorite.password);
      $("#login").click(); 
   }
}

function confirm_ticket() {
   $("#cancelBtn").click();
}

function retrieve_lists() {
   console.log("retrieve_lists begin");
   var lists = [];
   var fav_link = null;
   $("a[href]").each(function() {
      var href = $(this).attr("href");
      if (favorite.department == href) {
         fav_link = $(this)[0];
      }
      var queryString = {};
      href.replace(
         new RegExp("([^?=&]+)(=([^&]*))?", "g"),
         function($0, $1, $2, $3) { queryString[$1] = $3; }
      );
      lists.push({
         name : queryString['officeName'],
         id: queryString['office_id'],
         type: queryString['noType'],
         href: href
      });
   });
   chrome.runtime.sendMessage({method: "saveLists", value: lists}, function(response) {
      console.log(response);
   });
   if (fav_link) {
      fav_link.click();
   }
   console.log("retrieve_lists end");
}

function do_action() {
   if (window.location.pathname.endsWith("outDoctorList.do")) {
      retrieve_tickets();
   } else if (window.location.pathname.endsWith("loginPage.do")) {
      login();
   } else if (window.location.pathname.endsWith("actualGhPage.do")) {
      confirm_ticket();
   } else if (window.location.pathname.endsWith("gh.do")) {
      retrieve_lists();
   }
}

chrome.runtime.sendMessage({method: "getFavorite"}, function(response) {
   favorite = response;
   console.log(favorite);
   do_action();
});

console.log("path:" + window.location.pathname);
do_action();





