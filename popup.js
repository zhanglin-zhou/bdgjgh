function save_options() {
   var username = document.getElementById("username").value;
   var password = document.getElementById("password").value;
   var e = document.getElementById("department");
   var fav_department = e.options[e.selectedIndex].value;;
   var fav_doctor = document.getElementById("doctor").value;
   var fav_date = document.getElementById("date").value;

   localStorage["username"] = username.trim();
   localStorage["password"] = password.trim();
   localStorage["department"] = fav_department.trim();
   localStorage["doctor"] = fav_doctor.trim();
   localStorage["date"] = fav_date.trim();

}

function load_options() {
   var username = localStorage["username"];
   var password = localStorage["password"];
   var fav_department = localStorage["department"];
   var fav_doctor = localStorage["doctor"];
   var fav_date = localStorage["date"];
   var lists = JSON.parse(localStorage["lists"]);
   if (username != undefined) {
      document.getElementById("username").value = username;
   } else {
      document.getElementById("username").value = "";
   }
   if (password != undefined) {
      document.getElementById("password").value = password;
   } else {
      document.getElementById("password").value = "";
   }

   $.each(lists, function(index, value) {
      $("#department").append($('<option>', { value : value['href']}).text("" + value['name'] + (value['type'] == 1 ? "   普通" : "   专家")));
   });

   if (fav_department != undefined) {
      document.getElementById("department").value = fav_department;
   }

   if (fav_doctor != undefined) {
      document.getElementById("doctor").value = fav_doctor;
   } else {
      document.getElementById("doctor").value = "";
   }
   if (fav_date != undefined) {
      document.getElementById("date").value = fav_date;
   } else {
      document.getElementById("date").value = "";
   }
   document.getElementById("save").addEventListener("click",save_options);
}

window.addEventListener("load", load_options);